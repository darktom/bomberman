# Bomberman

## Spent Time
 - Interactables	1h
 - Pickups			1h
 - CharacterManager	7h
 - BaseCharacter	5h
 - GameMode	& WBP	1h
 - Board			3h
 - Bomb				3h

Total: 21h


## Roadmap if continues
 - Add FX
 - SFX and BSO
 - More pickups and power ups
 - Level by win rating
 - Elo and competitive
 - 2 - 4 Players per game
