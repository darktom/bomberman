// Copyright Epic Games, Inc. All Rights Reserved.

#include "BMBChar.h"

#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

#include "BMBActionBomb.h"
#include "BMBGameMode.h"
#include "Interfaces/BMBInteractable.h"

ABMBChar::ABMBChar()
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->bRunPhysicsWithNoController = true; // Allow to move character from external pawn

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ABMBChar::OnOverlapBegin);

	AmountOfBombs = 1;
	bIsRemotedBombActive = false;
	BombRadius = 2;
}


void ABMBChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsMoving && TargetDestination != FVector::ZeroVector) {
		LerpTime = LerpTime + DeltaTime;
		SetActorLocation(FMath::Lerp(TargetSource, TargetDestination, LerpTime / MovementSpeed));
		if (LerpTime >= MovementSpeed) {
			TargetDestination, TargetSource = FVector::ZeroVector;
			bIsMoving = false;
			LerpTime = 0;
			PrimaryActorTick.SetTickFunctionEnable(false);
		}
	}
}

void ABMBChar::MakeAction()
{
	if (AmountOfBombs > 0)
	{
		if (bIsRemotedBombActive && RemoteBomb)
		{
			RemoteBomb->Blast();
		}
		else
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;


			FVector TargetLocation = UKismetMathLibrary::Vector_SnappedToGrid(GetActorLocation(), GridSize / 2);

			ABMBActionBomb* Bomb = GetWorld()->SpawnActor<ABMBActionBomb>(BombSubclass, TargetLocation, FRotator::ZeroRotator, SpawnParams);
			Bomb->SetInstigator(GetInstigator());
			if (Bomb)
			{
				AmountOfBombs--;
				Bomb->OnBlastDelegate.AddDynamic(this, &ABMBChar::OnBlastedBomb);
				Bomb->InitBomb(bIsRemotedBombActive ? 0 : BaseTimeToBlastBomb, BombRadius, GridSize);

				if (bIsRemotedBombActive)
				{
					RemoteBomb = Bomb;
				}
			}
		}
	}
}

void ABMBChar::MoveForward(float Value)
{
	if (Value != 0.0f && !bIsMoving)
	{
		FHitResult Hit;
		FVector Start = GetActorLocation();
		Start.Z = GridSize / 2;
		FVector End = Start + FVector(GridSize * Value, 0, 0);
		FCollisionQueryParams CollisionParams;
		CollisionParams.AddIgnoredActor(this);

		UKismetSystemLibrary::DrawDebugLine(GetWorld(), Start, End, FColor::Red, 2.f, 2.f);

		GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_WorldDynamic, CollisionParams);

		if (!Hit.bBlockingHit)
			//AddActorLocalOffset(FVector(Value * GridSize, 0, 0));
			MoveTo(FVector(Value * GridSize, 0, 0));
	}
}

void ABMBChar::MoveRight(float Value)
{
	if (Value != 0.0f && !bIsMoving)
	{
		FHitResult Hit;
		FVector Start = GetActorLocation();
		Start.Z = GridSize / 2;
		FVector End = Start + FVector(0, GridSize * Value, 0);
		FCollisionQueryParams CollisionParams;
		CollisionParams.AddIgnoredActor(this);

		UKismetSystemLibrary::DrawDebugLine(GetWorld(), Start, End, FColor::Red, 2.f, 2.f);

		GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_WorldDynamic, CollisionParams);

		if (!Hit.bBlockingHit)
			MoveTo(FVector(0, Value * GridSize, 0));
	}
}

void ABMBChar::OnPickupActived(EPowerUpType PickupType)
{
	switch (PickupType)
	{
	case EPowerUpType::LongBlast:
		BombRadius++;
		break;
	case EPowerUpType::MoreBombs:
		AmountOfBombs++;
		break;
	case EPowerUpType::MoveSpeed:
		Cast<UCharacterMovementComponent>(GetMovementComponent())->MaxWalkSpeed = 600.0f;
		break;
	case EPowerUpType::RemoteBomb:
		bIsRemotedBombActive = true;
		GetWorld()->GetTimerManager().SetTimer(RemoteBombTH, this, &ABMBChar::OnPickupEnded, RemoteBombTimeToEnd, false);
		break;
	case EPowerUpType::NONE:
	default:
		break;
	}
}

void ABMBChar::OnBlastedBomb()
{
	AmountOfBombs++;
}

void ABMBChar::OnPickupEnded()
{
	bIsRemotedBombActive = false;

	if (RemoteBomb)
	{
		RemoteBomb->Blast();
	}
}

void ABMBChar::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	IBMBInteractable* idxInteractable = Cast<IBMBInteractable>(OtherActor); // Implements the interface?
	if (idxInteractable)
	{
		// Activate powerup!
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("It's a power up!"));
		PlayPickupFX();
		OnPickupActived(idxInteractable->GetPowerUp()); // Activate power up
		OtherActor->Destroy();
	}
}

void ABMBChar::ReceiveHit(int Index)
{
	bIsAlive = false;

	//OnDieDelegate.Broadcast(CharacterID);
}

void ABMBChar::MoveTo(FVector TargetLocation)
{
	TargetDestination = TargetLocation + GetActorLocation();
	TargetSource = GetActorLocation();
	bIsMoving = true;
	PrimaryActorTick.SetTickFunctionEnable(true);
}
