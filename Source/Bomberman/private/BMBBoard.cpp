#include "BMBBoard.h"

#include "Components/BillboardComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"

#include "BMBPickup.h"

ABMBBoard::ABMBBoard()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	BoardBillboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard Comp"));
	BoardBillboard->SetupAttachment(RootComponent);

	PermanentBlocks = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Permanent Blocks"));
	PermanentBlocks->SetupAttachment(RootComponent);
	PermanentBlocks->SetMobility(EComponentMobility::Movable);
	PermanentBlocks->SetCollisionObjectType(ECC_WorldDynamic);
	PermanentBlocks->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PermanentBlocks->SetCollisionResponseToAllChannels(ECR_Block);

	PermanentBlocks->ComponentTags.Add(TEXT("Wall"));
	PermanentBlocks->ComponentTags.Add(TEXT("Permanent"));

	DestructibleBlocks = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("Destructible Blocks"));
	DestructibleBlocks->SetupAttachment(RootComponent);
	DestructibleBlocks->SetMobility(EComponentMobility::Movable);
	DestructibleBlocks->SetCollisionObjectType(ECC_WorldDynamic);
	DestructibleBlocks->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	DestructibleBlocks->SetCollisionResponseToAllChannels(ECR_Block);
	DestructibleBlocks->ComponentTags.Add(TEXT("Wall"));

	Player1Spawn = CreateDefaultSubobject<USceneComponent>(TEXT("Player 1 Spawn"));
	Player1Spawn->SetupAttachment(PermanentBlocks);

	Player2Spawn = CreateDefaultSubobject<USceneComponent>(TEXT("Player 2 Spawn"));
	Player2Spawn->SetupAttachment(PermanentBlocks);

	//Initialize Board
	BoardSize = (IsEvenNumber(BoardSize)) ? BoardSize + 1 : BoardSize;
}

void ABMBBoard::ReceiveHit(int Index)
{
	if (Index != -1)
	{
		FTransform InstanceTransform;
		DestructibleBlocks->GetInstanceTransform(Index, InstanceTransform);
		InstanceTransform.SetLocation(DestructibleBlocks->GetComponentLocation() + InstanceTransform.GetLocation());
		DestructibleBlocks->RemoveInstance(Index);

		float rand = UKismetMathLibrary::RandomFloatInRange(0, 1);
		//if (rand <= .3f)
		//{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		ABMBPickup* Pickup = GetWorld()->SpawnActor<ABMBPickup>(PickupClass, InstanceTransform.GetLocation(), FRotator::ZeroRotator, SpawnParams);
		//}

	}
}

void ABMBBoard::BeginPlay()
{
	Super::BeginPlay();
	GenerateBoard();
}

bool ABMBBoard::IsPerimeterBlock(FVector2D Location) const
{
	if ((int)Location.Y % (BoardSize - 1) == 0 || ((int)Location.X % (BoardSize - 1)) == 0) {
		return true;
	}
	return false;
}

bool ABMBBoard::IsEvenNumber(int Value) const
{
	return (Value & 1) ? false : true;
}

bool ABMBBoard::IsSafeZone(float X, float Y) const
{
	// Player 1
	if ((X == 1 && Y == 1) || (X == 1 && Y == 2) || (X == 2 && Y == 1)) {
		return true;
	}
	// Player 2 
	if ((X == BoardSize - 2 && Y == BoardSize - 3) || (X == BoardSize - 2 && Y == BoardSize - 2) || (X == BoardSize - 3 && Y == BoardSize - 2)) {
		return true;
	}
	return false;
}

void ABMBBoard::GenerateBoard()
{
	//Calculate offset for center the instances
	PermanentBlocks->SetRelativeLocation(FVector(-(SectionSize * BoardSize) / 2, -(SectionSize * BoardSize) / 2, (SectionSize) / 2));
	DestructibleBlocks->SetRelativeLocation(FVector(-(SectionSize * BoardSize) / 2, -(SectionSize * BoardSize) / 2, (SectionSize) / 2));

	//Clear board
	PermanentBlocks->ClearInstances();
	DestructibleBlocks->ClearInstances();

	for (int X = 0; X < BoardSize; X++) {
		for (int Y = 0; Y < BoardSize; Y++) {
			FVector TargetLocation = FVector(X * SectionSize, Y * SectionSize, 0);

			if (IsPerimeterBlock(FVector2D(X, Y))) {
				PermanentBlocks->AddInstance(FTransform(TargetLocation));
			}
			else {
				if (IsEvenNumber(X) && IsEvenNumber(Y)) {
					PermanentBlocks->AddInstance(FTransform(TargetLocation));
				}
				else {
					float MinRange = -1.f;
					float MaxRange = .5f;

					float TemporalPerlin = FMath::PerlinNoise2D(FVector2D(FMath::FRandRange(MinRange, MaxRange), FMath::FRandRange(MinRange, MaxRange)));

					if (!IsSafeZone(X, Y))
					{
						if (TemporalPerlin > 0) {
							DestructibleBlocks->AddInstance(FTransform(TargetLocation));
						}
					}
					else
					{
						// UP - LEFT CORNER
						if (X == 1 && Y == 1) {
							UKismetSystemLibrary::PrintString(GetWorld(), TargetLocation.ToString());

							Player1Spawn->SetRelativeTransform(FTransform(TargetLocation));
						}
						else
						{
							// DOWN RIGHT CORNER
							if (X == BoardSize - 2 && Y == BoardSize - 2)
							{
								UKismetSystemLibrary::PrintString(GetWorld(), TargetLocation.ToString());
								Player2Spawn->SetRelativeTransform(FTransform(TargetLocation));
							}
						}
					}
				}
			}
		}
	}
}

void ABMBBoard::GenerateEditorBoard()
{
	GenerateBoard();
}

void ABMBBoard::ClearEditorBoard()
{
	PermanentBlocks->ClearInstances();
	DestructibleBlocks->ClearInstances();
}
