#include "BMBActionBomb.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"

#include "Interfaces/BMBInteractable.h"
#include "BMBBoard.h"
#include "BMBCharManager.h"

ABMBActionBomb::ABMBActionBomb()
{
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("Root component"));
	RootComponent = SphereComp;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh comp"));
	StaticMeshComp->SetupAttachment(RootComponent);
}

void ABMBActionBomb::InitBomb(int BlastingTime, int BombRadius, int SizeGrid)
{
	GridSize = SizeGrid;
	TimeToBlast = BlastingTime;
	ActionRadius = BombRadius;

	if (BlastingTime > 0) // Not Remote bomb
	{
		GetWorld()->GetTimerManager().SetTimer(CountDownTH, this, &ABMBActionBomb::OnCountDown, 1.f, true);
	}

	UKismetSystemLibrary::PrintString(GetWorld(), "Bomb location " + GetActorLocation().ToString());
}

void ABMBActionBomb::OnCountDown()
{
	TimeToBlast--;

	if (TimeToBlast <= 0)
	{
		if (CountDownTH.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(CountDownTH);
		}
		Blast();
	}
	else
	{
		OnPlayCountDownFX();
	}
}

void ABMBActionBomb::Blast()
{
	CheckForBlastObjetives(FVector(1, 0, 0));
	CheckForBlastObjetives(FVector(-1, 0, 0));
	CheckForBlastObjetives(FVector(0, 1, 0));
	CheckForBlastObjetives(FVector(0, -1, 0));

	// Apply Damage
	if (CountDownTH.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(CountDownTH);
	}

	OnPlayBlastFX();

	OnBlastDelegate.Broadcast();
	Destroy();
}

void ABMBActionBomb::ReceiveHit(int Index)
{
	Blast();
}

void ABMBActionBomb::CheckForBlastObjetives(FVector FireDirection)
{
	TArray<FHitResult> Hit;
	FVector Start = GetActorLocation();
	Start.Z = GridSize / 2;
	FVector End = Start + (GridSize * ActionRadius) * FireDirection.GetClampedToMaxSize(1);
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);

	UKismetSystemLibrary::DrawDebugLine(GetWorld(), Start, End, FColor::Green, 2.f, 2.f);

	GetWorld()->LineTraceMultiByChannel(Hit, Start, End, ECC_WorldDynamic, CollisionParams);

	for (FHitResult idx : Hit)
	{
		if (idx.bBlockingHit)
		{
			if (!idx.GetComponent()->ComponentHasTag(TEXT("Permanent")))
			{
				IBMBHittable* Hitted = Cast<IBMBHittable>(idx.Actor);
				if (Hitted)
				{
					Hitted->ReceiveHit(idx.Item);
				}
			}

			if (idx.GetComponent()->ComponentHasTag(TEXT("Wall")))
			{
				return;
			}
		}
	}

	Cast<ABMBCharManager>(GetInstigator())->CheckCharactersIntegrity();
}