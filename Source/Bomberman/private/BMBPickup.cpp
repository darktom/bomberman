#include "BMBPickup.h"

#include "Components/StaticMeshComponent.h"

ABMBPickup::ABMBPickup()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root component"));

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh component"));
	StaticMeshComp->SetupAttachment(RootComponent);

}

void ABMBPickup::BeginPlay()
{
	Super::BeginPlay();
}

EPowerUpType ABMBPickup::GetPowerUp() const
{
	return PowerUp;
}