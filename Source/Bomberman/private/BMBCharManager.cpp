#include "BMBCharManager.h"

#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "BMBChar.h"
#include "BMBBoard.h"
#include "BMBGameMode.h"
#include "Kismet/GameplayStatics.h"

ABMBCharManager::ABMBCharManager()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComp->SetupAttachment(RootComponent);
	SpringArmComp->SetUsingAbsoluteRotation(true);
	SpringArmComp->bDoCollisionTest = false;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(SpringArmComp, USpringArmComponent::SocketName);
	CameraComp->bUsePawnControlRotation = false;

	TargetCharacter1Class = TargetCharacter2Class = ABMBChar::StaticClass();
	BoardTargetClass = ABMBBoard::StaticClass();
	// Main configuration made in BP derivated class!
}

void ABMBCharManager::BeginPlay()
{
	Super::BeginPlay();

	if (!BoardGenerator)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		FVector TargetLocation = GetActorLocation();
		TargetLocation.Z = 0;
		BoardGenerator = GetWorld()->SpawnActor<ABMBBoard>(BoardTargetClass, TargetLocation, FRotator::ZeroRotator, SpawnParams);
	}

	InitCharacters();
}

void ABMBCharManager::InitCharacters()
{
	if (TargetCharacter1Class)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Character1 = GetWorld()->SpawnActor<ABMBChar>(TargetCharacter1Class, BoardGenerator->Player1Spawn->GetComponentLocation(), FRotator::ZeroRotator, SpawnParams);
		Character1->SetInstigator(this);
		Character1->GridSize = BoardGenerator->SectionSize;
		Character1->CharacterID = 0;
		Character1->OnDieDelegate.AddDynamic(this, &ABMBCharManager::OnAnyPlayerDies);
		if (bTwoPlayersControl)
		{
			Character2 = GetWorld()->SpawnActor<ABMBChar>(TargetCharacter2Class, BoardGenerator->Player2Spawn->GetComponentLocation(), FRotator::ZeroRotator, SpawnParams);
			Character2->SetInstigator(this);
			Character2->GridSize = BoardGenerator->SectionSize;
			Character2->CharacterID = 1;
			Character2->OnDieDelegate.AddDynamic(this, &ABMBCharManager::OnAnyPlayerDies);
		}
	}
}

void ABMBCharManager::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward1", this, &ABMBCharManager::MoveForwardFirst);
	PlayerInputComponent->BindAxis("MoveRight1", this, &ABMBCharManager::MoveRightFirst);
	PlayerInputComponent->BindAction("Action1", EInputEvent::IE_Pressed, this, &ABMBCharManager::ActionFirst);

	PlayerInputComponent->BindAxis("MoveForward2", this, &ABMBCharManager::MoveForwardSecond);
	PlayerInputComponent->BindAxis("MoveRight2", this, &ABMBCharManager::MoveRightSecond);
	PlayerInputComponent->BindAction("Action2", EInputEvent::IE_Pressed, this, &ABMBCharManager::ActionSecond);
}

void ABMBCharManager::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	float TargetZoom = ABMBGameMode::CalculateZoomBetweenTwoPlayers(Character1, Character2, 800.f, 300.f);
	SpringArmComp->TargetArmLength = TargetZoom;
}

void ABMBCharManager::MoveForwardFirst(float AxisValue)
{
	if (AxisValue != 0 && Character1)
	{
		Character1->MoveForward(AxisValue);
	}
}

void ABMBCharManager::MoveRightFirst(float AxisValue)
{
	if (AxisValue != 0 && Character1)
	{
		Character1->MoveRight(AxisValue);
	}
}

void ABMBCharManager::ActionFirst()
{
	if (Character1)
	{
		Character1->MakeAction();
	}
}

void ABMBCharManager::MoveForwardSecond(float AxisValue)
{
	if (AxisValue != 0 && Character2)
	{
		Character2->MoveForward(AxisValue);
	}
}

void ABMBCharManager::MoveRightSecond(float AxisValue)
{
	if (AxisValue != 0 && Character2)
	{
		Character2->MoveRight(AxisValue);
	}
}

void ABMBCharManager::ActionSecond()
{
	if (Character2)
	{
		Character2->MakeAction();
	}
}

void ABMBCharManager::CheckCharactersIntegrity()
{
	ABMBGameMode* GameMode = Cast<ABMBGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (!GameMode)
		return;

	if (!Character1->bIsAlive && !Character2->bIsAlive)
	{
		GameMode->OnDrawResult();
	}
	else
	{
		if (!Character1->bIsAlive || !Character2->bIsAlive)
		{
			FString CharacterStr = (Character1->bIsAlive ? "0" : "1");
			GameMode->OnCharacterDies("Character " + CharacterStr);
		}
	}
}

void ABMBCharManager::OnAnyPlayerDies(uint8 PlayerID)
{
	ABMBGameMode* GameMode = Cast<ABMBGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GameMode)
	{
		if (!Character1->bIsAlive && !Character2->bIsAlive)
		{
			GameMode->OnDrawResult();
		}
		else
		{

			FString CharacterStr = (PlayerID == 1 ? "0" : "1");
			GameMode->OnCharacterDies("Character " + CharacterStr);
		}
	}
}