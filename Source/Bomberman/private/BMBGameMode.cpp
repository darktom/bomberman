// Copyright Epic Games, Inc. All Rights Reserved.


#include "BMBGameMode.h"

#include "GameFramework/Character.h"

float ABMBGameMode::CalculateZoomBetweenTwoPlayers(ACharacter* Char1, ACharacter* Char2, float SafeZoomDistance, float MaxZoomDistance)
{
	if (!Char1 || !Char2)
		return 0;

	FVector Char1Location = Char1->GetActorLocation();
	FVector Char2Location = Char2->GetActorLocation();

	// Calculate the Height triangle (60-30) for the zoom
	float ZoomAmount = 0;
	ZoomAmount = FVector::Distance(Char1Location, Char2Location) / 2;
	ZoomAmount *= FMath::Sqrt(3);
	ZoomAmount += SafeZoomDistance;

	return FMath::Clamp(ZoomAmount, MaxZoomDistance, ZoomAmount);
}