#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BMBCharManager.generated.h"

UCLASS()
class BOMBERMAN_API ABMBCharManager : public APawn
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* SpringArmComp;

	ABMBCharManager();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void Tick(float DeltaSeconds) override;

	void CheckCharactersIntegrity();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<class ABMBChar> TargetCharacter1Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<class ABMBChar> TargetCharacter2Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		TSubclassOf<class ABMBBoard> BoardTargetClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		class ABMBChar* Character1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		class ABMBChar* Character2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		bool bTwoPlayersControl = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		class ABMBBoard* BoardGenerator;

	virtual void BeginPlay() override;

	void InitCharacters();

	// First character bindings
	void MoveForwardFirst(float AxisValue);
	void MoveRightFirst(float AxisValue);
	void ActionFirst();

	// Second character bindings
	void MoveForwardSecond(float AxisValue);
	void MoveRightSecond(float AxisValue);
	void ActionSecond();


	UFUNCTION()
		void OnAnyPlayerDies(uint8 PlayerID);
};
