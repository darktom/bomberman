// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/BMBInteractable.h"
#include "Interfaces/BMBHittable.h"
#include "BMBActionBomb.h"
#include "BMBChar.generated.h"

UCLASS(Blueprintable)
class ABMBChar : public ACharacter, public IBMBHittable
{

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDieSignature, uint8, PlayerID);

	GENERATED_BODY()

public:

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FOnDieSignature OnDieDelegate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		float CooldownRemainingTime = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		uint8 BombRadius = 2;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		bool bIsRemotedBombActive = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		const TSubclassOf<ABMBActionBomb> BombSubclass = ABMBActionBomb::StaticClass();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Setup")
		int AmountOfBombs = 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		bool bIsAlive = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		bool bIsMoving = false;

	float GridSize;
	uint8 CharacterID;

	ABMBChar();
	virtual void Tick(float DeltaTime) override;
	void ReceiveHit(int Index);
	void MoveForward(float Value);
	void MoveRight(float Value);
	void OnPickupActived(EPowerUpType PickupType);

	UFUNCTION(BlueprintCallable)
		void MakeAction();

	UFUNCTION()
		void OnBlastedBomb();

	UFUNCTION()
		void OnPickupEnded();

	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent)
		void PlayPickupFX();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ABMBActionBomb* RemoteBomb;

	const int RemoteBombTimeToEnd = 10;
	const int BaseTimeToBlastBomb = 5;

private:

	FTimerHandle RemoteBombTH;
	FVector TargetDestination;
	FVector TargetSource;
	float MovementSpeed = .2f;
	float LerpTime = .0f;

	void MoveTo(FVector TargetLocation);
};

