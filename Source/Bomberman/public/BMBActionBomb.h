// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/BMBInteractable.h"
#include "Interfaces/BMBHittable.h"
#include "BMBActionBomb.generated.h"

UCLASS()
class BOMBERMAN_API ABMBActionBomb : public AActor, public IBMBHittable
{
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBlastSignature);

	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* SphereComp;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FOnBlastSignature OnBlastDelegate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int TimeToBlast;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int ActionRadius;

	ABMBActionBomb();
	void InitBomb(int BlastingTime, int BombRadius, int SizeGrid);
	void Blast();

	void ReceiveHit(int Index);
protected:

	FTimerHandle CountDownTH;
	int GridSize;

	UFUNCTION()
		void OnCountDown();

	UFUNCTION(BlueprintImplementableEvent)
		void OnPlayCountDownFX() const;

	UFUNCTION(BlueprintImplementableEvent)
		void OnPlayBlastFX() const;
	
	void CheckForBlastObjetives(FVector FireDirection);
};
