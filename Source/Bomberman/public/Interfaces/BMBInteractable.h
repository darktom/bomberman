// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BMBInteractable.generated.h"


UENUM(BlueprintType)
enum class EPowerUpType : uint8 {
	NONE = 0		UMETA(Hidden),
	LongBlast = 1	UMETA(DisplayName = "Long Blast"),	// 2 more cells
	MoreBombs = 2	UMETA(DisplayName = "More bomb"),	// 1 bomb more
	MoveSpeed = 3	UMETA(DisplayName = "Move speed"),	// 300+ move speed
	RemoteBomb = 4	UMETA(DisplayName = "Delay Bomb"),	// 10s time delayed
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBMBInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class BOMBERMAN_API IBMBInteractable
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPowerUpType GetPowerUp() const = 0;

};
