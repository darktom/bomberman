// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BMBHittable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UBMBHittable : public UInterface
{
	GENERATED_BODY()
};

class BOMBERMAN_API IBMBHittable
{
	GENERATED_BODY()

public:

	virtual void ReceiveHit(int Index) = 0;
	
};
