// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BMBGameMode.generated.h"

/**
 *
 */
UCLASS()
class BOMBERMAN_API ABMBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	static float CalculateZoomBetweenTwoPlayers(ACharacter* Char1, ACharacter* Char2, float SafeZoomDistance, float MaxZoomDistance);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnCharacterDies(const FString& CharacterID);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnDrawResult();
};
