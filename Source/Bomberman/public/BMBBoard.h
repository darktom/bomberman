// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/BMBHittable.h"
#include "BMBPickup.h"
#include "BMBBoard.generated.h"

UCLASS()
class BOMBERMAN_API ABMBBoard : public AActor, public IBMBHittable
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UInstancedStaticMeshComponent* PermanentBlocks;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UHierarchicalInstancedStaticMeshComponent* DestructibleBlocks;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UBillboardComponent* BoardBillboard;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		USceneComponent* Player1Spawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		USceneComponent* Player2Spawn;


	ABMBBoard();

	UFUNCTION(BlueprintCallable)
		void GenerateBoard();


	UFUNCTION(BlueprintCallable, CallInEditor)
		void GenerateEditorBoard();

	UFUNCTION(BlueprintCallable, CallInEditor)
		void ClearEditorBoard();

	// Defines the board dimension, is required to be a Odd number
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
		uint8 BoardSize = 13;

	// Section size in meters, by default 1 meter to match Unreal Engine SM_Cube 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
		float SectionSize = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup")
		TSubclassOf<ABMBPickup> PickupClass = ABMBPickup::StaticClass();

	void ReceiveHit(int Index);

protected:
	virtual void BeginPlay() override;

private:

	bool IsPerimeterBlock(FVector2D Location) const;
	bool IsEvenNumber(int Value) const;
	bool IsSafeZone(float x, float y) const;
};
