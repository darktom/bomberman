// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/BMBInteractable.h"
#include "BMBPickup.generated.h"

UCLASS()
class BOMBERMAN_API ABMBPickup : public AActor, public IBMBInteractable
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndEffectSignature);

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FOnEndEffectSignature OnEndEffectDelegate;

	ABMBPickup();
	virtual EPowerUpType GetPowerUp() const override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Setup") 
		EPowerUpType PowerUp = EPowerUpType::NONE; // Random value in BP!

	virtual void BeginPlay() override;
};
